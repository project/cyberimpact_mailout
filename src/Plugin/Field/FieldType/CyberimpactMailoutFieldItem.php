<?php

namespace Drupal\cyberimpact_mailout\Plugin\Field\FieldType;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Upanupstudios\Cyberimpact\Php\Client\Config;
use Upanupstudios\Cyberimpact\Php\Client\Cyberimpact;
use GuzzleHttp\Client;

/**
 * Plugin implementation of the 'cyberimpact_mailout' field type.
 *
 * @FieldType(
 *   id = "cyberimpact_mailout",
 *   label = @Translation("Cyberimpact mailout"),
 *   description = @Translation("Allows an entity to send an email to subscribers through Cyberimpact API."),
 *   default_widget = "cyberimpact_mailout_default"
 * )
 */
class CyberimpactMailoutFieldItem extends FieldItemBase {

  protected $mailout = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'subject' => '@title',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return array(
      'group_creation' => 1,
      'group_ids' => '',
      'group_select' => FALSE,
      'sendnow_label' => 'Send mailout to subscribers now',
      'sendnow_select_groups' => FALSE,
      'scheduled_mailout' => FALSE,
      'scheduled_mailout_label' => 'Schedule mailout to subscribers later',
      'mailout_details' => FALSE
    ) + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   * @todo: Not sure what to use this for...
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['recipients'] = DataDefinition::create('integer')
      ->setLabel(t('Mailout'))
      ->setDescription(t('Number of recipients.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   * @todo: Not sure what to use this for...
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'recipients' => [
          'type' => 'int',
          'size' => 'big',
          'not null' => TRUE,
          'default' => 0,
        ],
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Email subject'),
      '#description' => t('Use @type or @title field names for replacement'),
      '#default_value' => $this->getSetting('subject'),
      '#required' => TRUE
    );

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    $settings = \Drupal::config('cyberimpact_mailout.settings');
    $api_token = $settings->get('api_token');

    if(!empty($api_token)) {
      // Do we always need this - Yes, because we can't do depenency injection
      $config = new Config($api_token);
      $httpClient = new Client();
      $cyberimpact = new Cyberimpact($config, $httpClient);

      $options = [];
      $groups = $cyberimpact->groups()->getAll();

      foreach ($groups['groups'] as $group) {
        $options[$group['id']] = $this->t('@title (@members_count subscribers)', [
          '@title' => $group['title'],
          '@members_count' => $group['membersCount']
        ]);
      }

      $cyberimpact_url = Url::fromUri('https://app.cyberimpact.com/', array('attributes' => array('target' => '_blank')));

      $element['group_creation'] = [
        '#title' => $this->t('Groups'),
        '#type' => 'radios',
        '#default_value' => $this->getSetting('group_creation'),
        '#options' => array(
          0 => $this->t('Dynamically create and send mailout to a group using the node title for the single node'),
          1 => $this->t('Send mailout to the selected groups below for all nodes'),
        ),
        '#attributes' => [
          'class' => [
            'group-creation-radios',
          ],
        ],
        '#attached' => [
          'library' => [
            'cyberimpact_mailout/cyberimpact_mailout_fieldsettings'
          ]
        ]
      ];

      $element['group_ids'] = array(
        '#type' => 'select',
        '#multiple' => TRUE,
        '#description' => $this->t('Groups retrieved from @Cyberimpact.', array('@Cyberimpact' => Link::fromTextAndUrl('Cyberimpact', $cyberimpact_url)->toString())),
        '#options' => $options,
        '#default_value' => $this->getSetting('group_ids'),
      );

      $element['group_select'] = array(
        '#title' => 'Enable to select groups (from selected groups) before sending',
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('group_select'),
      );

      $element['sendnow_label'] = array(
        '#title' => 'Send now label',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('sendnow_label'),
      );

      $element['scheduled_mailout'] = array(
        '#title' => $this->t('Enable scheduled mailout'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('scheduled_mailout'),
        '#attributes' => [
          'class' => [
            'scheduled-mailout-checkbox',
          ],
        ],
      );

      $element['scheduled_mailout_label'] = array(
        '#title' => 'Scheduled mailout Label',
        '#type' => 'textfield',
        '#default_value' => $this->getSetting('scheduled_mailout_label'),
      );

      $element['mailout_details'] = array(
        '#title' => $this->t('Enable additional mailout details'),
        '#type' => 'checkbox',
        '#default_value' => $this->getSetting('mailout_details')
      );
    } else {
      $message = 'Missing Cyberimpact API Token! Enter the API Token in the @cyberimpact_settings.';
      $url = Url::fromRoute('cyberimpact_mailout.settings');

      $message = $this->t($message, [
        '@cyberimpact_settings' => Link::fromTextAndUrl(t('Cyberimpact settings'), $url)->toString(),
      ]);

      \Drupal::logger('cyberimpact_mailout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    parent::setValue($values, $notify);

    $this->recipients = 1;
    $values['recipients'] = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    parent::postSave($update);

    // Get cyberimpact settings
    $settings = \Drupal::config('cyberimpact_mailout.settings');
    $api_token = $settings->get('api_token');
    $debug_render_template = $settings->get('debug_render_template');
    $debug_sendto_group_id = $settings->get('debug_sendto_group_id');

    if(!empty($api_token)) {
      $config = new Config($api_token);
      $httpClient = new Client();
      $cyberimpact = new Cyberimpact($config, $httpClient);

      // Get entity
      $entity = $this->getEntity();
      $isPublished = $entity->isPublished();

      // Get entity properties
      $title = $entity->getTitle();
      $type = $entity->type->entity->label();

      // Cyberimpact groups
      $group = NULL;
      $group_ids = NULL;

      // Dynamic - Create group using node title
      if($this->getSetting('group_creation') == 0) {
        // Find the group in cyberimpact
        //TODO: We need to store and get the group ID so it will be sent to the same group
        // Even if the node title has changed..., also change the title...
        // if group id is known, get by id
        // otherwise, get by title
        $group = $cyberimpact->groups()->getByTitle($title);

        // if no group found, create it
        if(empty($group)) {
          $groupData = [
            'title' => $title,
            'isPublic' => TRUE
          ];

          $group = $cyberimpact->groups()->add($groupData);

          if(!empty($group) && empty($group['errors'])) {
            //TODO: Save the group ID in the field!
            $message = t('The Cyberimpact group @title has been created.', [
              '@title' => $group['title']
            ]);

            \Drupal::logger('cyberimpact_mailout')->notice($message);
            \Drupal::messenger()->addMessage($message, 'status', FALSE);
          } else {
            $errors = [];
            $message = $group['message'].' @errors';

            if(!empty($group['errors'])) {
              foreach($group['errors'] as $field_errors) {
                $errors[] = implode(' ', $field_errors);
              }
            }

            $message = t($message, [
              '@errors' => implode(' ', $errors)
            ]);

            \Drupal::logger('cyberimpact_mailout')->error($message);
            \Drupal::messenger()->addMessage($message, 'error', FALSE);
            return;
          }
        }
      }

      if($isPublished && (!empty($this->sendnow) || !empty($this->sendlater))) {
        // Get the default theme and change it back to active rendering
        $config = \Drupal::config('system.theme');
        $default_theme = \Drupal::service('theme.initialization')->getActiveThemeByName($config->get('default'));
        $active_theme = \Drupal::theme()->getActiveTheme();

        \Drupal::theme()->setActiveTheme($default_theme);

        // Get email subject
        $subject = $this->getSetting('subject');

        if(empty($subject)) {
          // Set default subject if empty (string)
          $subject = $this->t('@type: @title')->render();
        }

        $subject = $this->t($subject, array('@type' => $type, '@title' => $title));

        // html decode subject
        $subject = htmlspecialchars_decode($subject);

        // Get date fields and correct the time
        $fields = $entity->getFields();
        $timezone = date_default_timezone_get();

        if(!empty($fields)) {
          foreach($fields as $field) {
            $field_type = $field->getFieldDefinition()->getType();

            if(in_array($field_type, array('date', 'datetime', 'date_recur'))) {
              $datetime_type = $field->getFieldDefinition()->getSetting('datetime_type');

              if($datetime_type == 'datetime') {
                $field_name = $field->getName();

                $value = new \DateTime($entity->$field_name->value, new \DateTimeZone('UTC'));
                $value->setTimezone(new \DateTimeZone($timezone));
                $entity->$field_name->value = sprintf("%sT%s", $value->format('Y-m-d'), $value->format('H:i:s'));

                $end_value = new \DateTime($entity->$field_name->end_value, new \DateTimeZone('UTC'));
                $end_value->setTimezone(new \DateTimeZone($timezone));
                $entity->$field_name->end_value = sprintf("%sT%s", $end_value->format('Y-m-d'), $end_value->format('H:i:s'));
              }
            }
          }
        }

        // Prep body content
        $entity->body->value = $this->prepContent($entity->body->value);

        // Prep detail content
        $details['value'] = $this->prepContent($this->details['value']);

        // Render template
        $template = [
          '#theme' => 'cyberimpact_mailout',
          '#entity' => $entity,
          '#details' => $details
        ];

        $body = \Drupal::service('renderer')->render($template);

        if(!empty($debug_render_template)) {
          echo $body;

          //TODO: Die and hide errors for now, need a better way to debug the template for development
          error_reporting(0);
          die();
        }

        // Restore active theme
        \Drupal::theme()->setActiveTheme($active_theme);

        if(!empty($debug_sendto_group_id)) {
          $group_ids = $debug_sendto_group_id;
        } else {
          // Get group ids
          if(empty($this->getSetting('group_creation'))) {
            // Dynamically created group
            if(!empty($group)) {
              $group_ids = $group['id'];
            }
          } else {
            if(empty($this->getSetting('group_select'))) {
              $group_ids = $this->getSetting('group_ids');
              $group_ids = implode(',', array_filter($group_ids));
            } else {
              if(!empty($this->group_ids)) {
                $group_ids = implode(',', array_filter($this->group_ids));
              }
            }
          }
        }

        // Get group name(s)
        $group_names = [];
        $groups = $cyberimpact->groups()->getAll();

        if(!empty($groups['groups'])) {
          foreach ($groups['groups'] as $group) {
            if(in_array($group['id'], explode(',', $group_ids))) {
              $group_names[$group['id']] = $group['title'];
            }
          }
        }

        // Determine date
        $datetime = new \DateTime('now');

        if(!empty($this->sendlater)) {
          $datetime = new \DateTime($this->sendlater_datetime['object']->__toString());
        }

        // Sender settings
        $sender_from_name = $settings->get('sender_from_name');
        $sender_from_email = $settings->get('sender_from_email');
        $sender_replyto_email = $settings->get('sender_replyto_email');

        if(empty($sender_replyto_email)) {
          $sender_replyto_email = $sender_from_email;
        }

        // Prepare mailout attributes
        $mailingData = [
          'subject' => $subject,
          'language' => 'en_ca',
          'groups' => $group_ids,
          'sendAt' => $datetime->format('Y-m-d H:i:s'),
          'sendSpeed' => 'regular',
          'replyTo' => $sender_replyto_email,
          'fromName' => $sender_from_name,
          'mailFrom' => $sender_from_email,
          'bodyHtml' => $body,
          'bodyText' => $body,
          'googleAnalytics' => TRUE,
          'googleAnalyticsCampaign' => $subject,
          'sendToAFriend' => FALSE
        ];

        $mailing = $cyberimpact->mailings()->create($mailingData);

        if (!empty($mailing) && is_array($mailing)) {
          if(!empty($this->sendnow)) {
            $message = 'The "@subject" has been sent now to @groups groups.';
          } elseif(!empty($this->sendlater)) {
            $message = 'The "@subject" will be sent on @date to @groups groups.';
          }

          $message = $this->t($message, [
            '@subject' => $subject,
            '@date' => $datetime->format('F j, Y g:ia'),
            '@recipients' => $mailing['recipientsCount'],
            '@groups' => implode(', ', $group_names)
          ]);

          \Drupal::logger('cyberimpact_mailout')->notice($message);
          \Drupal::messenger()->addMessage($message, 'status', FALSE);
        } else {
          \Drupal::logger('cyberimpact_mailout')->error($mailing);
          \Drupal::messenger()->addMessage($mailing, 'error', FALSE);
        }
      }
    } else {
      $message = 'Missing Cyberimpact API Token! Enter the API Token in the @cyberimpact_settings.';
      $url = Url::fromRoute('cyberimpact_mailout.settings');

      $message = t($message, [
        '@cyberimpact_settings' => Link::fromTextAndUrl(t('Cyberimpact settings'), $url)->toString(),
      ]);

      \Drupal::logger('cyberimpact_mailout')->notice($message);
      \Drupal::messenger()->addMessage($message, 'error', FALSE);
    }
  }

  public function prepContent($content) {
    if(is_string($content)) {
      // Get current schema and host
      $scheme_and_host = \Drupal::request()->getSchemeAndHttpHost();

      // Rewrite relative urls with current scheme and domain in links
      preg_match_all('#<a\s.*?(?:href=[\'"](.*?)[\'"]).*?>#is', $content, $matches);

      if(!empty($matches)) {
        foreach($matches[1] as $match) {
          if(preg_match("/^\//", $match)) {
            $content = str_replace($match, $scheme_and_host.$match, $content);
          }
        }
      }

      // Rewrite relative url with current scheme and domain in images
      preg_match_all('#<img\s.*?(?:src=[\'"](.*?)[\'"]).*?/>#is', $content, $matches);

      if(!empty($matches)) {
        foreach($matches[1] as $match) {
          if(preg_match("/^\//", $match)) {
            $content = str_replace($match, $scheme_and_host.$match, $content);
          }
        }
      }
    }

    return $content;
  }
}