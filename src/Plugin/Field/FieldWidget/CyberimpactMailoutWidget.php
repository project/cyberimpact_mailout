<?php

namespace Drupal\cyberimpact_mailout\Plugin\Field\FieldWidget;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Upanupstudios\Cyberimpact\Php\Client\Config;
use Upanupstudios\Cyberimpact\Php\Client\Cyberimpact;
use GuzzleHttp\Client;

/**
 * Plugin implementation of the 'cyberimpact_mailout_default' widget.
 *
 * @FieldWidget(
 *   id = "cyberimpact_mailout_default",
 *   label = @Translation("Mailout"),
 *   field_types = {
 *     "cyberimpact_mailout"
 *   },
 *   settings = {
 *     "placeholder" = "Select Cyberimpact groups(s)."
 *   }
 * )
 */
class CyberimpactMailoutWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $settings = \Drupal::config('cyberimpact_mailout.settings');

    $api_token = $settings->get('api_token');
    $debug_render_template = $settings->get('debug_render_template');
    $debug_sendto_group_id = $settings->get('debug_sendto_group_id');

    if(!empty($api_token)) {
      $config = new Config($api_token);
      $httpClient = new Client();
      $cyberimpact = new Cyberimpact($config, $httpClient);

      $groups = $cyberimpact->groups()->getAll();
      $group_ids = $this->getFieldSetting('group_ids');

      if(!empty($debug_render_template)) {
        $message = 'Render email template on screen when publishing a node is enabled. Change in @cyberimpact_settings.';
        $url = Url::fromRoute('cyberimpact_mailout.settings');

        $message = t($message, [
          '@cyberimpact_settings' => Link::fromTextAndUrl(t('Cyberimpact settings'), $url)->toString(),
        ]);

        \Drupal::messenger()->addMessage($message, 'warning', FALSE);
      }

      if(!empty($debug_sendto_group_id)) {
        $group_name = 'selected group';

        if(!empty($groups)) {
          foreach($groups['groups'] as $group) {
            if($group['id'] == $debug_sendto_group_id) {
              $group_name = $group['title'];
            }
          }
        }

        $message = 'All mailings will be sent to @group_name for testing. Change in @cyberimpact_settings.';
        $url = Url::fromRoute('cyberimpact_mailout.settings');

        $message = t($message, [
          '@group_name' => $group_name,
          '@cyberimpact_settings' => Link::fromTextAndUrl(t('Cyberimpact settings'), $url)->toString(),
        ]);

        \Drupal::messenger()->addMessage($message, 'warning', FALSE);
      }

      $element['sendnow'] = array(
        '#title' => $this->getFieldSetting('sendnow_label'),
        '#type' => 'checkbox',
        '#attributes' => [
          'class' => [
            'sendnow-checkbox',
          ],
        ],
        '#attached' => [
          'library' => [
            'cyberimpact_mailout/cyberimpact_mailout_widget'
          ]
        ]
      );

      if(!empty($this->getFieldSetting('group_select'))) {
        $options = [];

        foreach ($groups['groups'] as $group) {
          if(in_array($group['id'], $group_ids)) {
            $options[$group['id']] = $this->t('@title (@members_count subscribers)', [
              '@title' => $group['title'],
              '@members_count' => $group['membersCount']
            ]);
          }
        }

        $element['group_ids'] = array(
          '#type' => 'checkboxes',
          '#description' => $this->t('Select groups to send mailout'),
          '#options' => $options,
          '#attributes' => [
            'class' => [
              'sendnow-groups',
            ],
          ]
        );
      }

      if(!empty($this->getFieldSetting('scheduled_mailout'))) {
        $element['sendlater'] = array(
          '#title' => $this->getFieldSetting('scheduled_mailout_label'),
          '#type' => 'checkbox',
          '#attributes' => [
            'class' => [
              'sendlater-checkbox',
            ],
          ]
        );

        $element['sendlater_datetime'] = [
          '#title' => $this->t('Schedule date &amp; time'),
          '#type' => 'datetime',
          '#attributes' => [
            'class' => [
              'sendlater-datetime',
            ],
          ],
          '#element_validate' => [
            [static::class, 'validate'],
          ],
        ];
      }

      if($this->getFieldSetting('mailout_details')) {
        $element['details'] = [
          '#title' => $this->t('Mailout details'),
          '#type' => 'text_format',
          '#attributes' => [
            'class' => [
              'mailout-details-text',
            ],
          ],
        ];
      }
    }

    return $element;
  }

  /**
   * Validate the datetime field.
   */
  public static function validate($element, FormStateInterface $form_state) {
    // Get the field name
    $field_name = current($element['#parents']);

    // Get value
    $value = $form_state->getValue($field_name);

    if(!empty($value[0]['sendlater'])) {
      $sendlater_datetime = array_filter($value[0]['sendlater_datetime']);

      if(!empty($sendlater_datetime) && !empty($sendlater_datetime['date']) && !empty($sendlater_datetime['time'])) {
        // Check if it's greater than the date and time now
        // Note: the site's timezone is used to convert the time
        $now = new DrupalDateTime();
        $datetime = $sendlater_datetime['object'];

        if($datetime < $now) {
          $form_state->setError($element, t("Schedule must be set in the future."));
        }
      } else {
        $form_state->setError($element, t("Enter scheduled date and time."));
      }
    }
  }

}