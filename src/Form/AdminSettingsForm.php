<?php

namespace Drupal\cyberimpact_mailout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannel;
use Upanupstudios\Cyberimpact\Php\Client\Config;
use Upanupstudios\Cyberimpact\Php\Client\Cyberimpact;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

/**
 * Configure Cyberimpact Mailout settings
 */
class AdminSettingsForm extends ConfigFormBase {

   /**
   * Messenger service.
   *
   * @var Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerChannel $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')->get('cyberimpact_mailout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cyberimpact_mailout_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cyberimpact_mailout.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Default settings
    $settings = $this->config('cyberimpact_mailout.settings');

    $api_token = $settings->get('api_token');
    $api_token_url = Url::fromUri('https://app.cyberimpact.com/config/api-tokens', ['attributes' => array('target' => '_blank')]);

    // API token
    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('API'),
      '#open' => empty($api_token)
    ];
    $form['api']['api_token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API Token'),
      '#default_value' => $api_token,
      '#description' => $this->t('The API token for your Cyberimpact account. Get or generate a valid API key at your @api_token_url.', [
        '@api_token_url' => Link::fromTextAndUrl(t('API tokens page'), $api_token_url)->toString()
      ])
    ];

    if(!empty($api_token)) {
      $config = new Config($api_token);
      $httpClient = new Client();
      $cyberimpact = new Cyberimpact($config, $httpClient);

      $sender_addresses_url = Url::fromUri('https://app.cyberimpact.com/config/custom-email-froms', ['attributes' => array('target' => '_blank')]);
      $sender_from_name = $settings->get('sender_from_name');
      $sender_from_email = $settings->get('sender_from_email');
      $sender_replyto_email = $settings->get('sender_replyto_email');

      $form['sender'] = [
        '#type' => 'details',
        '#title' => $this->t('Sender Information'),
        '#open' => TRUE,
        '#description' => $this->t('Enter the sender information to be used when sending mailings')
      ];
      $form['sender']['sender_from_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('From Name'),
        '#default_value' => $sender_from_name
      ];
      $form['sender']['sender_from_email'] = [
        '#type' => 'email',
        '#title' => $this->t('From Email'),
        '#default_value' => $sender_from_email,
        '#description' => $this->t('Make sure that the email is valid and is in the list of @sender_addresses_link in the Cyberimpact settings page.', [
          '@sender_addresses_link' => Link::fromTextAndUrl(t('Sender addresses'), $sender_addresses_url)->toString()
        ])
      ];
      $form['sender']['sender_replyto_email'] = [
        '#type' => 'email',
        '#title' => $this->t('Reply-to Email'),
        '#default_value' => $sender_replyto_email,
        '#description' => $this->t('Leave empty to use from email')
      ];

      $debug_render_template = $settings->get('debug_render_template');
      $debug_sendto_group_id = $settings->get('debug_sendto_group_id');

      $groups = $cyberimpact->groups()->getAll();
      $options[0] = $this->t('- Use field settings -');

      foreach ($groups['groups'] as $group) {
        $options[$group['id']] = $this->t('@title (@members_count subscribers)', [
          '@title' => $group['title'],
          '@members_count' => $group['membersCount']
        ]);
      }

      $form['debug'] = [
        '#type' => 'details',
        '#title' => $this->t('Debug'),
        '#open' => (!empty($debug_render_template) || !empty($debug_sendto_group_id)),
      ];
      $form['debug']['debug_render_template'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Render email template on screen when publishing a node'),
        '#default_value' => $debug_render_template
      ];
      $form['debug']['debug_sendto_group_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Send all mailings to the selected group for testing'),
        '#options' => $options,
        '#default_value' => $debug_sendto_group_id,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $api_token = $form_state->getValue('api_token');

    if(!empty($api_token)) {
      $config = new Config($api_token);
      $httpClient = new Client();
      $cyberimpact = new Cyberimpact($config, $httpClient);

      $response = $cyberimpact->ping();

      if(empty($response['ping']) || (!empty($response['ping']) && $response['ping'] != 'success')) {
        $form_state->setErrorByName('api_token', $this->t($response));
        $this->logger->error($response);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('cyberimpact_mailout.settings');

    $api_token = $form_state->getValue('api_token');

    $settings->set('api_token', $api_token)
      ->save();

    $sender_from_name = $form_state->getValue('sender_from_name');
    $sender_from_email = $form_state->getValue('sender_from_email');
    $sender_replyto_email = $form_state->getValue('sender_replyto_email');

    $settings->set('sender_from_name', $sender_from_name)
      ->set('sender_from_email', $sender_from_email)
      ->set('sender_replyto_email', $sender_replyto_email)
      ->save();

    $debug_render_template = $form_state->getValue('debug_render_template');
    $debug_sendto_group_id = $form_state->getValue('debug_sendto_group_id');

    $settings->set('debug_render_template', $debug_render_template)
      ->set('debug_sendto_group_id', $debug_sendto_group_id)
      ->save();


    return parent::submitForm($form, $form_state);
  }

}
