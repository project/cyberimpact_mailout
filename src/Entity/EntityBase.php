<?php

namespace Drupal\cyberimpact_mailout\Entity;

use Upanupstudios\Cyberimpact\Php\Client\Config;
use Upanupstudios\Cyberimpact\Php\Client\Cyberimpact;

/**
 * Provides the base class for Cyberimpact entities.
 */
class EntityBase {

  /**
   * Cyberimpact client.
   *
   * @var Upanupstudios\Perfectmind\Php\Client\Perfectmind
   */
  protected $cyberimpact;

  /**
   * Constructs a new Cyberimpact object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The surrey perfectmind configuration.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend instance to use.
   */
  public function __construct() {
    $settings = \Drupal::config('surrey_perfectmind.settings');

    $api_url = $settings->get('api_url');
    $client_number = $settings->get('client_number');
    $api_key = $settings->get('api_key');

    $config = new Config($api_url, $client_number, $api_key);
    $httpClient = new EEnClient();

    $this->perfectmind = new Perfectmind($config, $httpClient);
  }