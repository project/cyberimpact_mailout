(function ($, Drupal)
{
  Drupal.behaviors.cyberimpact_mailout_widget = {
    attach: function (context, settings)
    {
      if($('.sendnow-checkbox').is(':checked')) {
        $('.sendnow-groups').show();
      } else {
        $('.sendnow-groups').hide();
      }

      if($('.sendlater-checkbox').is(':checked')) {
        $('.sendlater-datetime').parent('.form-datetime-wrapper').show();
      } else {
        $('.sendlater-datetime').parent('.form-datetime-wrapper').hide();
      }

      $('.sendnow-checkbox').click(function() {
        if($(this).is(':checked')) {
          $('.sendlater-checkbox').prop('checked', false);
          $('.sendlater-datetime').parent('.form-datetime-wrapper').hide();
        }

        if($(this).is(':checked')) {
          $('.sendnow-groups').show();
        } else {
          $('.sendnow-groups').hide();
        }
      });

      $('.sendlater-checkbox').click(function() {
        if($(this).is(':checked')) {
          $('.sendnow-checkbox').prop('checked', false);
          $('.sendlater-datetime').parent('.form-datetime-wrapper').show();
        } else {
          $('.sendlater-datetime').parent('.form-datetime-wrapper').hide();
        }
      });
    }
  };
}(jQuery, Drupal));