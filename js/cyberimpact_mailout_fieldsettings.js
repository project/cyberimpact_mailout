(function ($, Drupal)
{
  Drupal.behaviors.cyberimpact_mailout_fieldsettings = {
    attach: function (context, settings)
    {
      if($('#edit-settings-group-creation-0').is(':checked')) {
        $('div.js-form-item-settings-group-ids').hide();
      } else {
        $('div.js-form-item-settings-group-ids').show();
      }

      $('input.group-creation-radios').click(function() {
        if($(this).val() == 0) {
          $('div.js-form-item-settings-group-ids').hide();
          $('div.js-form-item-settings-group-select').hide();
        } else {
          $('div.js-form-item-settings-group-ids').show();
          $('div.js-form-item-settings-group-select').show();
        }
      });

      if($('#edit-settings-scheduled-mailout').is(':checked')) {
        $('div.js-form-item-settings-scheduled-mailout-label').show();
      } else {
        $('div.js-form-item-settings-scheduled-mailout-label').hide();
      }

      $('#edit-settings-scheduled-mailout').click(function() {
        if($(this).is(':checked')) {
          $('div.js-form-item-settings-scheduled-mailout-label').show();
        } else {
          $('div.js-form-item-settings-scheduled-mailout-label').hide();
        }
      });
    }
  };
}(jQuery, Drupal));