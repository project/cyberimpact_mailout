CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration

INTRODUCTION
------------



INSTALLATION
------------

Install the Cyberimpact Mailout module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420
for further information.


CONFIGURATION
-------------

TODO:
Debug mode to display template
Set group to send to in debug mode
Add notice with link to setting
Global Subject header template